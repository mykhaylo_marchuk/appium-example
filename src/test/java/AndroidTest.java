import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class AndroidTest {
    private AndroidDriver driver;

    @BeforeTest
    public void setupDriver() {
        AndroidComponent android = new AndroidComponent();
        DesiredCapabilities capabilities = android.getAVDCapabilities();
        driver = android.getAndroidDriver(capabilities);
    }

    @Test
    public void androidTest() {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
}
