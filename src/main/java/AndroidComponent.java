import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

public class AndroidComponent {
    public DesiredCapabilities getAVDCapabilities() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1.1");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Nexus S API 25");
        capabilities.setCapability(MobileCapabilityType.APPLICATION_NAME, "com.android.browser");
        return capabilities;
    }

    public AndroidDriver getAndroidDriver(DesiredCapabilities caps) {
        URL url;
        try {
            url = new URL("http://127.0.0.1:4732/wd/hub/");
        }
        catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }

        return new AndroidDriver(url, caps);
    }
}
